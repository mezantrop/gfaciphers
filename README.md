# Russian Gravity Falls Ciphers / Шифры Гравити Фолз

<a href="https://www.buymeacoffee.com/mezantrop" target="_blank"><img src="https://cdn.buymeacoffee.com/buttons/default-orange.png" alt="Buy Me A Coffee" height="41" width="174"></a>

## Caesar, Atbash, Vigenere for kids

Очень простая реализация алгоритмов кодирования Атбаш, Цезаря и Виженера для разгадывания кодов в **русскоязычном** Дневнике №3 неизветного автора Гравити Фолз.

[Щелкни здесь, чтобы запустить шифровальную машину Online](https://mezantrop.github.io/ancient_ciphers.html)

Или если у тебя есть Python 3, запусти:
```
	ancient_ciphers.py a|c|v "Текст" "Ключ"

Укажи "а", "c" или "v" чтобы выбрать метод кодирования: Атбаш, Цезаря или Виженера
```

Например:
```
./ancient_ciphers.py v "Яртпцв, дтэбфр т ъощбх!" "Пайнс"
Код Виженера: привет, диппер и мэйбл!
```
